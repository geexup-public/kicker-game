import { Injectable } from '@angular/core';
import { CoreService } from './core.service';
import { Player } from 'src/core/src';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  maxPlayers = 4;
  players: Array<Player> = [];

  constructor(
    private coreService: CoreService
  ) {
    // Save link
    this.players = coreService.game.playground.players;
  }

  newPlayer(name: string): void {
    const player = new Player(name);
    this.coreService.game.addPlayers(player);
  }

  deletePlayer(player: Player): void {
    const index = this.players.findIndex(item => item === player);
    this.players.splice(index, 1);
  }
}
