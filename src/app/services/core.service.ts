import { Injectable } from '@angular/core';
import { Game, GameState, Player } from 'src/core/src';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public game: Game;
  public stateChange = new Subject<GameState>();

  constructor() {
    this.game = new Game();
    this.game.state.subscribe(state => this.stateChange.next(state));

    // TODO: remove
    this.game.addPlayers(new Player('test'));
    this.game.addPlayers(new Player('test2'));
    this.game.addPlayers(new Player('test3'));
    this.game.addPlayers(new Player('test4'));
    this.game.doReady();
  }
}
