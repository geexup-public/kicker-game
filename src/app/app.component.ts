import NoSleep from 'nosleep.js';

import { Component } from '@angular/core';
import { CoreService } from './services/core.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  step = 'step1';
  noSleep = new NoSleep();

  constructor(
    private core: CoreService
  ) {}

  setStep(step: 'step1' | 'step2' | 'step3'): void {
    this.step = step;
  }

  step2(): void {
    this.setStep('step2');
    this.core.game.doReady();
  }

  step3(): void {
    this.noSleep.enable();
    this.setStep('step3');
    this.core.game.start();
  }
}
