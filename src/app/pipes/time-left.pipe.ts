import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeLeft'
})
export class TimeLeftPipe implements PipeTransform {
  transform(value: number): string {
    if (!value) {
      return '00:00';
    }

    const seconds = Math.floor((value / 1000) % 60);
    const minutes = Math.floor((value / (1000 * 60)) % 60);

    return ((minutes < 10) ? '0' + minutes : minutes)
      + ':'
      + ((seconds < 10) ? '0' + seconds : seconds);
  }
}
