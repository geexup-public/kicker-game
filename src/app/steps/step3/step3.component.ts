import { Component, OnInit } from '@angular/core';
import { Howl } from 'howler';

import { CoreService } from 'src/app/services/core.service';
import { GameState } from 'src/core/src';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  timeCount = 1;
  sound: Howl;

  constructor(
    public core: CoreService
  ) { }

  isPlaying(): boolean {
    return this.core.game.state.value === GameState.IN_PROGRESS;
  }

  isPlayerChangeTime(): boolean {
    return this.core.game.state.value === GameState.TIMER_POSITION_CHANGE;
  }

  pause(): void {
    this.core.game.pause();
  }

  play(): void {
    this.core.game.play();
  }

  doChange(): void {
    this.timeCount++;
    this.core.game.play();
  }

  ngOnInit(): void {
    this.sound = new Howl({
      src: ['/assets/sound.mp3']
    });

    this.core.game.state.subscribe(state => {
      if (state === GameState.TIMER_POSITION_CHANGE) {
        this.playSound();
      }
    });
  }

  playSound(): void {
    this.sound.play();
  }
}
