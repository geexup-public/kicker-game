import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PlayersService } from 'src/app/services/players.service';
import { MatDialog } from '@angular/material/dialog';
import { PlayerModalComponent } from './player-modal/player-modal.component';
import { Player } from 'src/core/src';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  @Output()
  next = new EventEmitter();

  constructor(
    public playersService: PlayersService,
    private matDialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  newPlayerModal(): void {
    const ref = this.matDialog.open(PlayerModalComponent, {});
    ref.afterClosed().subscribe((name: string) => {
      if (name) {
        this.playersService.newPlayer(name);
      }
    });
  }

  deletePlayer(player: Player): void {
    this.playersService.deletePlayer(player);
  }
}
