import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';
import { PlaygroundComponent } from 'src/app/components/playground/playground.component';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  @Output()
  next = new EventEmitter();

  constructor(
    private core: CoreService
  ) { }

  ngOnInit(): void {
  }

  resort(playgroundView: PlaygroundComponent): void {
    this.core.game.playground.randomizePositions();
    playgroundView.getPlayers();
  }
}
