import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';
import { GameState } from 'src/core/src';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-control-buttons',
  templateUrl: './control-buttons.component.html',
  styleUrls: ['./control-buttons.component.scss']
})
export class ControlButtonsComponent implements OnInit, OnDestroy {
  public gameStates = GameState;
  public state: GameState;

  private subscription: Subscription;

  constructor(
    public coreService: CoreService
  ) { }

  ngOnInit(): void {
    this.state = this.coreService.game.state.value;
    this.subscription = this.coreService.stateChange.subscribe(state => this.state = state);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
