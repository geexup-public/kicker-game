import { Component, OnInit } from '@angular/core';
import { Player } from 'src/core/src';
import { CoreService } from 'src/app/services/core.service';
import { Subscription } from 'rxjs';
import { Team } from 'src/core/src/definitions/team';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {
  player00: Player;
  team00: Team;
  player01: Player;
  team01: Team;
  player10: Player;
  team10: Team;
  player11: Player;
  team11: Team;

  subscription: Subscription;

  constructor(
    public coreService: CoreService
  ) {
  }

  getPlayers(): void {
    this.coreService.game.playground.playersPositions.forEach(pp => {
      if (pp.position.coordinate.x === 0 && pp.position.coordinate.y === 0) {
        this.player00 = pp.player;
        this.team00 = pp.position.team;
      }

      if (pp.position.coordinate.x === 0 && pp.position.coordinate.y === 1) {
        this.player01 = pp.player;
        this.team01 = pp.position.team;
      }

      if (pp.position.coordinate.x === 1 && pp.position.coordinate.y === 0) {
        this.player10 = pp.player;
        this.team10 = pp.position.team;
      }

      if (pp.position.coordinate.x === 1 && pp.position.coordinate.y === 1) {
        this.player11 = pp.player;
        this.team11 = pp.position.team;
      }
    });
  }

  ngOnInit(): void {
    this.getPlayers();
  }
}
