import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimerComponent implements OnInit {
  leftTime: number;

  constructor(
    public coreService: CoreService,
    private chDRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    setInterval(() => {
      this.leftTime = this.coreService.game.timer.timeLeft;
      this.chDRef.detectChanges();
    }, 500);
  }
}
