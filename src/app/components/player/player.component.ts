import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/core/src';
import { Team } from 'src/core/src/definitions/team';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  @Input()
  player: Player;

  @Input()
  team: Team;

  @Input()
  controllers = false;

  @Output()
  delete = new EventEmitter<void>();

  ngOnInit(): void {}
}
